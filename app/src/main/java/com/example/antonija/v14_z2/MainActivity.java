package com.example.antonija.v14_z2;

import android.app.Activity;
import android.media.Image;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import java.util.Random;

public class MainActivity extends Activity implements View.OnClickListener {

    ImageView iAlanTuring;
    ImageView iBillGates;
    ImageView iNoamChomsky;
    Random rndgenerator = new Random();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.initalizeUI();
    }

    private void initalizeUI()
    {
        this.iAlanTuring=(ImageView) findViewById(R.id.iAlanTuring);
        this.iBillGates=(ImageView) findViewById(R.id.iBillGates);
        this.iNoamChomsky=(ImageView) findViewById(R.id.iNoamChomsky);
        this.iAlanTuring.setOnClickListener(this);
        this.iBillGates.setOnClickListener(this);
        this.iNoamChomsky.setOnClickListener(this);
    }

    public String generateQuoteAT(Random rndgenerator)
    {
        String []quoteArrayAT = getResources().getStringArray(R.array.AlanTuring);
        int random = rndgenerator.nextInt(quoteArrayAT.length);
        return quoteArrayAT[random];
    }
    public String generateQuoteBG(Random rndgenerator)
    {
        String []quoteArrayBG = getResources().getStringArray(R.array.BillGates);
        int random = rndgenerator.nextInt(quoteArrayBG.length);
        return quoteArrayBG[random];
    }
    public String generateQuoteNC(Random rndgenerator)
    {
        String []quoteArrayNC = getResources().getStringArray(R.array.NoamChomsky);
        int random = rndgenerator.nextInt(quoteArrayNC.length);
        return quoteArrayNC[random];
    }
    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.iAlanTuring:
                String quote = this.generateQuoteAT(rndgenerator);
                Toast.makeText(this, quote, Toast.LENGTH_LONG).show();
                break;
            case R.id.iBillGates:
                quote = this.generateQuoteBG(rndgenerator);
                Toast.makeText(this, quote, Toast.LENGTH_LONG).show();
                break;
            case R.id.iNoamChomsky:
                quote = this.generateQuoteNC(rndgenerator);
                Toast.makeText(this, quote, Toast.LENGTH_LONG).show();
                break;
        }
    }
}
